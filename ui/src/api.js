const axios = require("axios");

const targetURL = "http://localhost:5000/api/scooters/find";

export default {
  findScooters: (body) => {
    return axios.post(targetURL, body)
    .then(res => res.data)
    .catch(err => console.log(err))
  }
};
