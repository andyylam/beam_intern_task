import React from 'react';
import Form from './components/Form'
import Map from './components/Map'

class App extends React.Component {
  state = { scooters: [], location: {} }

  setScooters = scooters => {
    return this.setState({ scooters })
  }

  setLocation = location => {
    this.setState({location})
  }

  render() {
    return (
      <div style={{ height: '100vh', width: '100%' }}>
        <div style={{
          position:'fixed',
          zIndex: 1,
          top: 20,
          left: 20,
          width: '30%',
          height: 'auto',
          "borderRadius": 8,
          "boxShadow": "0 13px 27px -5px rgba(50, 50, 93, 0.25), 0 8px 16px -8px rgba(0, 0, 0, 0.3)",
          "padding":"10px 20px",
          backgroundColor:'white'
        }}>
          <Form setScooters={this.setScooters} setLocation={this.setLocation}/>
        </div>  
        <Map scooters={this.state.scooters} location={this.state.location} />
      </div>
    );
  }
}

export default App;
