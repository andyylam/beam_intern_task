import React from 'react'
import {
  Form,
  Input,
  Button,
} from 'antd';
import 'antd/dist/antd.css'; 
import api from '../../api'

class ScooterForm extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      const {x_scooters ,y_meters ,lat ,lon} = values
      if (!err && x_scooters && y_meters && lat && lon) {
        api.findScooters(values)
          .then(res => {
            this.props.setLocation({lat, lon})
            this.props.setScooters(res)})
          .catch(err => console.log(err))
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit} >
        <h1 style={{textAlign: 'center'}}>Query scooters</h1>
        <Form.Item label="Number of scooters">
          {getFieldDecorator('x_scooters', {
            rules: [
              {
                required: true,
                message: 'Please input number of scooters',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Maximum distance">
          {getFieldDecorator('y_meters', {
            rules: [
              {
                required: true,
                message: 'Please input distance',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Latitude">
          {getFieldDecorator('lat', {
            rules: [
              {
                required: true,
                message: 'Please input a latitude',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Longitude">
          {getFieldDecorator('lon', {
            rules: [
              {
                required: true,
                message: 'Please input a longitude',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Search nearest scooters
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default Form.create({ name: 'register' })(ScooterForm);
