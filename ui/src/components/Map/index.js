import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

class Map extends Component {
  state = {map: null, maps: null, markers: []}

  static defaultProps = {
    center: {
      lat: 1.3521,
      lng: 103.8198
    },
    zoom: 11
  };

  componentDidUpdate = (prevProps) => {
    if (prevProps !== this.props) {
      this.clearMarkers()
      this.renderMarkers()
      this.renderLocation()
    }
  }

  renderLocation = () => {
    const { lat, lon } = this.props.location
    const marker = new this.state.maps.Marker({
      map: this.state.map,
      position: new this.state.maps.LatLng(lat, lon ),
      label: 'HERE',
    }); 
    this.setState(prevState => ({...prevState, markers: [...prevState.markers, marker]}))
  }

  clearMarkers = () => {
    for (let i = 0; i < this.state.markers.length; i++) {
      this.state.markers[i].setMap(null)
    }
    this.setState({markers: []})
  }

  handleApi = (map, maps) => {
    this.setState({map, maps})
  }

  renderMarkers = () => {
    const { scooters } = this.props;
    return scooters.forEach((scooter, index) => {
      const marker = new this.state.maps.Marker({
        map: this.state.map,
        position: new this.state.maps.LatLng(scooter.loc.coordinates[1], scooter.loc.coordinates[0]),
        label: '' + index,
      }); 
      this.setState(prevState => ({...prevState, markers: [...prevState.markers, marker]}))
    })
  }

  render() {
    return (
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: process.env.REACT_APP_MAPS_API_KEY }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          yesIWantToUseGoogleMapApiInternals={true}
          onGoogleApiLoaded={({ map, maps }) => this.handleApi(map, maps)}
        >
        </GoogleMapReact>
      </div>
    );
  }
}

export default Map;