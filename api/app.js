const cors = require("cors");
const express = require("express");
const morgan = require("morgan");
const winston = require("./winston");
const routes = require("./routes");

require("dotenv").config();

// Create the Express app.
const app = express();

// Enable All CORS Requests
app.use(cors());

// Setup request body JSON parsing.
app.use(express.json({ limit: "5mb" }));

// Setup morgan which gives us HTTP request logging.
app.use(morgan("combined", { stream: winston.stream }));

var mongoose = require("mongoose");

//Address of current atlas Database
mongoose.connect(process.env.DB_CONNECTION_STRING, {
	useUnifiedTopology: true,
	useNewUrlParser: true
});

var db = mongoose.connection;

db.on("error", err => {
	console.error("connection error: ", err);
});

db.once("open", () => {
	console.log("MongoDB connection successful");
});

// Add routes.
app.use("/api", routes);

// Send 404 if no other route matched.
app.use((req, res) => {
	res.status(404).json({
		message: "Route Not Found"
	});
});

// Setup a global error handler.
app.use((err, req, res, next) => {
	console.error(`Global error handler: ${JSON.stringify(err.stack)}`);
	winston.error(
		`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${
			req.method
		} - ${req.ip}`
	);

	res.status(err.status || 500).json({
		message: err.message,
		error: process.env.NODE_ENV === "production" ? {} : err
	});
	res.render("error");
});

// Set our port.
app.set("port", process.env.PORT || 5000);

// Start listening on our port.
const server = app.listen(app.get("port"), () => {
	console.log(`Express server is listening on port ${server.address().port}`);
});
