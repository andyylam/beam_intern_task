const express = require("express");

// Construct a router instance.
const router = express.Router();
const scooterController = require("./scooters.controller");

// Route for retrieving all scooters
router.get("/", scooterController.retrieveAllScooters);

// Route for creating a new scooter
router.post("/", scooterController.createNewScooter);

router.post("/find", scooterController.findScooters);

module.exports = router;
