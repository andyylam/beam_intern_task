const Scooter = require("./scooters.model");

// Retrieve all current scooters
const retrieveAllScooters = (req, res, next) => {
	return Scooter.find({}).exec((err, scooters) => {
		if (err) return next(err);
		res.json(scooters);
	});
};

// Create a new scooter
const createNewScooter = (req, res, next) => {
	console.log(req.body);
	let scooter = new Scooter(req.body);
	return scooter.save((err, scooter) => {
		if (err) return next(err);
		res.status(201);
		res.json(scooter);
	});
};

const findScooters = (req, res, next) => {
	const x_scooters = req.body.x_scooters;
	const y_meters = req.body.y_meters;
	const lat = req.body.lat;
	const lon = req.body.lon;

	Scooter.find(
		{
			loc: {
				$nearSphere: {
					$geometry: {
						type: "Point",
						coordinates: [lon, lat]
					},
					$minDistance: 0,
					$maxDistance: y_meters
				}
			}
		},
		null,
		{ limit: parseInt(x_scooters) },
		(err, scooters) => {
			if (err) return next(err);
			res.status(201);
			res.json(scooters);
		}
	);
};

module.exports = {
	retrieveAllScooters,
	createNewScooter,
	findScooters
};
