const mongoose = require("mongoose");

const ScooterSchema = new mongoose.Schema({
	loc: { type: { type: String }, coordinates: [Number] }
});

ScooterSchema.index({ loc: "2dsphere" });

const Scooter = mongoose.model("Scooter", ScooterSchema);
module.exports = Scooter;
