const express = require("express");

// Import routers
const scooterRouter = require("./entities/scooters/scooters.routes");

// Construct a router instance.
const router = express.Router();

router.use("/scooters", scooterRouter);

module.exports = router;
