import random
import requests
import json

# Choa Chu Kang: 1.375719, 103.739564
# Geylang: 1.313441, 103.906414
# range:
# 	latitude 1.313441 - 1.375719
# 	longitude 103.739564 - 103.906414

header = {"Content-Type": "application/json"}
for i in range(100):
    lat = random.randint(1313441, 1375719) / 10**6
    lon = random.randint(103739564, 103906414) / 10**6
    payload = json.dumps({"loc": {"type": "Point", "coordinates": [lon, lat]}})
    r = requests.post("http://localhost:5000/api/scooters",
                      data=payload, headers=header)
    print(r.status_code, r.reason)
