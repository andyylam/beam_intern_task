# Beam Intern Exercise

Code a system that fetches the closest X scooters for a given lat, lon within y metres, and plot it onto a map. This exercise includes front-end, back-end, and data storage, and you can choose any technology you like for each.

To accomplish the task, I used ReactJS for front-end, ExpressJS for back-end and MongoDB for database. All code is written in JavaScript, except for a python script to randomly generate scooters.

### Prerequisites

```
The latest version of NodeJS installed on your computer
```

### Installing

1. Ensure that you have NodeJS installed on your computer.
2. Go into the api/ directory and run `npm i --save` on your terminal
3. Create a .env file with one line: `DB_CONNECTION_STRING=mongodb+srv://test:test@cluster0-pqav7.mongodb.net/test?retryWrites=true&w=majority
`
4. Run `npm run start` to start the local backend server on localhost:5000
5. Go into the ui/ directory and run `npm i --save` on your terminal
6. Generate a google maps API key at https://cloud.google.com/maps-platform/#get-started.
7. Create a .env file with one line: `REACT_APP_MAPS_API_KEY={your api key}`
8. Run `npm run start` to start the front end server. A browser should pop up with the application launched.
9. Input test fields:
    - x_scooter: 5
    - y_meters: 10000
    - lat: 1.3521
    - lon: 103.8198
